package com.lousite.example;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

import com.lousite.ErrorHandling.MyOwnException;


public class RecursionMiniExamples {
	private boolean Done = false;
	
	// phase one of printStars
	public void printStars1(int numberOfStars) {
		if(numberOfStars == 1) {
			// base case
			// easy to solve
			// do it ourselves
			System.out.print("*");
		}else {
			// recursive case
			// hard to solve
			// ask for help by making calls to ourself
			System.out.print("*");
			printStars(numberOfStars - 1);
		}
	}
	
	// phase two of printStars
	public void printStars2(int numberOfStars) {
		if(numberOfStars == 1) {
			//System.out.print("*");
		}else {
			System.out.print("*");
			printStars(numberOfStars - 1);
		}
	}
	
	// final phase of printStars
	public void printStars(int numberOfStars) {
		System.out.print("*");
		if(numberOfStars > 1) {
			printStars(numberOfStars - 1);
		}
	}	

	
	// phase one of power method
	public int power1 (int base, int exponent) throws Exception {
		if (exponent < 0) {
			throw new Exception("Negative exponent is not allowed: " + exponent);
		}
		
		if (exponent == 0) {
			// Base Case
			return 1;
		}else {
			// recursive case
			// 3^7 == (3^6) * (3^1) == 2,187
			return base * power1(base, exponent -1);
			// 3 * answer power(3, 7-1)   3 * 729 (result of below object) = 2187
			// 3 * answer power(3, 6-1)   3 * 243 (result of below object) =  729
			// 3 * answer power(3, 5-1)   3 * 81  (result of below object) =  243
			// 3 * answer power(3, 4-1)   3 * 27  (result of below object) =   81      
			// 3 * answer power(3, 3-1)   3 * 9   (result of below object) =   27
			// 3 * answer power(3, 2-1)   3 * 3   (result of below object) =    9
			// 3 * answer power(3, 1-1)   3 * 1   (result of below object) =    3
			// This object executed base case which returns 1
		}
	}
	
	
	// This is not recursion, this is read file and write out to a new file in reverse order
	public void readFileAndWriteInReverseOrder (BufferedReader fileReader, BufferedWriter fileWriter) {
            
		    String line;
			try {
				line = fileReader.readLine();
				if(line != null) {
			    	try {
			    		System.out.println("Input: " + line);
			    		readFileAndWriteInReverseOrder(fileReader, fileWriter);
			    		System.out.println("Output: " + line);
						fileWriter.write(line + "\n");
					} catch (IOException e) {
						throw new MyOwnException("A write error occurred.", e);
					}			    	
			    }
				if(!Done) {
					Done = true;
					System.out.println("===========================================");
				}				
			} catch (IOException e1) {
				throw new MyOwnException("A read error occurred.", e1);
			}
	}
}
