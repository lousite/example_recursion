package com.lousite.ErrorHandling;

public class MyOwnException 
extends RuntimeException {
  public MyOwnException(String errorMessage, Throwable err) {
      super(errorMessage, err);
  }
}
