package com.lousite.application;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.lousite.example.RecursionMiniExamples;


public class Exec_PrintTheFileInDescendingOrder {
	
	public static void main(String[] args) throws IOException {
		ReadFileAndWriteToNewFileInReverseOrder();
	
	}    

	
	public static void ReadFileAndWriteToNewFileInReverseOrder() throws IOException {
		final String INPUT_FILE_NAME = "src//main//resources//inputfile.txt";
	    final String OUTPUT_FILE_NAME = "src//main//resources//outputfile.txt";
	    //final Charset ENCODING_TYPE = StandardCharsets.UTF_8;
	    FileReader myFileReader = new FileReader(INPUT_FILE_NAME);
	    BufferedReader fileReader = new BufferedReader(myFileReader);
	    BufferedWriter fileWriter = setupOutputFileForWriting(OUTPUT_FILE_NAME);
	    RecursionMiniExamples example2 = new RecursionMiniExamples();
	    

	    //read file into stream, try-with-resource  			
		example2.readFileAndWriteInReverseOrder(fileReader, fileWriter);
	    
	    closeOutputFile(fileWriter);
		
	}

	
	public static BufferedWriter setupOutputFileForWriting(String fileName) 
			  throws IOException {
			    BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
			    //writer.write(str);
			     
			    //writer.close();
			    return writer;
			}

	
	public static void closeOutputFile(BufferedWriter writer) 
			  throws IOException {
    
			    writer.close();
			}

}

